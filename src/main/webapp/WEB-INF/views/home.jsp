<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="true"%>
<!DOCTYPE html>
<html>
<head>
<title>Guesto</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="/resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Guesto -->
<link href="/resources/css/guesto.css" rel="stylesheet">

<!-- Pine alerts -->
<link href="/resources/css/jquery.pnotify.default.css" rel="stylesheet">
<link href="/resources/css/jquery.pnotify.default.icons.css" rel="stylesheet">
<link href="/resources/css/nprogress.css" rel="stylesheet">

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments);},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	</script>
</head>
<body data-ng-app="myModule">

	<div class="container-fluid">
		<div class="row">
			<div class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-responsive-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Guesto</a>
				</div>
				<div class="navbar-collapse collapse navbar-responsive-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a  class="dropdown-toggle"
							data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>
								<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/logout">Sign Out</a></li>
							</ul></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" style="margin-top: 25px;">
		<div class="row">
			<div class="col-lg-2 menu-column" data-ng-controller="NavController">
				<ul class="nav nav-pills nav-stacked " style="max-width: 300px;" >
					<li data-ng-class="{active:activeIndex==0}" data-ng-click="activeIndex=0" ><a href="#">Home</a></li>
					<li data-ng-class="{active:activeIndex==1}"><a class="dropdown-toggle"
						data-toggle="dropdown" > Guest <span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
							<li  data-ng-click="activeIndex=1"><a analytics-on="click" analytics-event="View" href="#/guest/add">Add</a></li>
							<li  data-ng-click="activeIndex=1"><a href="#/guest/list">List</a></li>
							<li  data-ng-click="activeIndex=1"><a href="#/guest/search">Search</a></li>
						</ul>
					</li>
					<li  data-ng-class="{active:activeIndex==2}" ><a class="dropdown-toggle"
						data-toggle="dropdown" > Expenses <span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
							<li  data-ng-click="activeIndex=2"><a href="#/expenses/add">Add</a></li>
							<li data-ng-click="activeIndex=2"><a href="#/expenses/list">List</a></li>
							<li  data-ng-click="activeIndex=2"><a href="#/expenses/search">Search</a></li>
						</ul>
					</li>
					<li  data-ng-class="{active:activeIndex==3}"><a class="dropdown-toggle"
						data-toggle="dropdown" > Contacts <span class="caret"></span>
					</a>
						<ul class="dropdown-menu">
							<li data-ng-click="activeIndex=3"><a href="#/contacts/import">Import From Google</a></li>
							<li data-ng-click="activeIndex=3"><a href="#/contacts/list">List</a></li>
							<li data-ng-click="activeIndex=3"><a href="#/contacts/search">Search</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="col-lg-10" >
				<div class="row" data-ng-view="" data-ng-animate="slide">
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="/resources/js/angular.min.js"></script>
	<script type="text/javascript" src="/resources/js/angular-route.min.js"></script>
	<script type="text/javascript"
		src="/resources/js/angular-animate.min.js"></script>
	<script type="text/javascript" src="/resources/js/ui-bootstrap.min.js"></script>
	<script type="text/javascript" src="/resources/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/resources/js/angular/app.js"></script>
	<script type="text/javascript" src="/resources/js/jquery.pnotify.min.js"></script>
	<script type="text/javascript" src="/resources/js/ngProgress.js"></script>
	
	<script type="text/javascript" src="/resources/js/angulartics.min.js"></script>
	<script type="text/javascript" src="/resources/js/angulartics-ga.min.js"></script>
	<script type="text/javascript" src="/resources/js/angulartics-scroll.min.js"></script>

</body>
</html>
