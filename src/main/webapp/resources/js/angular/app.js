var myModule = angular.module("myModule", [ "ngRoute" ,"ngProgress",'angulartics', 'angulartics.google.analytics']);


myModule.constant("gaUID","UA-49592460-1");

myModule.factory("__guestService", function($http) {
	return {
		list : function() {
			var promise = $http({
				method : 'GET',
				url : '/guest/list'
			});
			return promise;
		},
		save : function(formData) {
			var promise = $http({
				method : 'POST',
				url : '/guest/save',
				data : formData,
				headers : {
					'Content-Type' : 'application/json'
				}
			});
			return promise;
		},
		get : function(id){
			var promise = $http({
				method : 'GET',
				url : '/guest/detail/'+id,
				headers : {
					'Content-Type' : 'application/json'
				}
			});
			return promise;
		}
	};
});

myModule.controller("ListGuestController", function($scope, __guestService,ngProgress) {
	ngProgress.start();
	__guestService.list().then(function(data) {
		$scope.guestList = data.data;
	});
	$scope.orderBy  = "fullName";
	$scope.orderDirection = true;
	ngProgress.reset();
});

myModule.controller("AddGuestController", function($scope, __guestService,$location,$route) {

	$scope.formData = {};
	$scope.formData.guestType = "INDIVIDUAL";

	$scope.submit = function() {
		if ($scope.addGuestForm.$valid ) {
			var returnedPromise = __guestService.save($scope.formData);
			console.log(returnedPromise);
			returnedPromise.success(function(data, status){
				$.pnotify({
				    title: 'Guest Saved !!',
				    text: data.fullName +' added to guest list.',
				    type: 'success',
				    opacity:0.8
				});
				$location.path('/guest/add');
				$route.reload();
			}).error(function(data,status){
				if(status=409){
					$.pnotify({
						title : 'Guest Not Saved !!',
						text : 'Conflicting data. Guest with same email/phone might exist.',
						type : 'error',
						opacity:0.8,
						icon :false
					});
				}
			});
		} else {
			console.log($scope.addGuestForm.$error);
		}
	};

	$scope.reset = function() {
		$location.path('/guest/add');
		$route.reload();
	};
});

myModule.controller("NavController", function($scope, $rootScope) {
	$rootScope.$on("$locationChangeStart", function(event, next) {
		if (next.indexOf("guest") > -1) {
			$scope.activeIndex = 1;
		} else if (next.indexOf("expenses") > -1) {
			$scope.activeIndex = 2;
		} else if (next.indexOf("contacts") > -1) {
			$scope.activeIndex = 3;
		} else {
			$scope.activeIndex = 0;
		}
	});
});

myModule.directive("guestDetail",function(){
	return {
		restrict	:	"A",
		templateUrl	:	"/resources/partials/guest/detail.html"
	};
});


myModule.controller("GuestDetailController", ['$scope','$routeParams','__guestService',function($scope,$routeParams,__guestService) {
	console.log($routeParams);
	__guestService.get($routeParams.id).then(function(data){
		$scope.guest = data;
		console.log(data);
	});
}]);

myModule.config([ "$routeProvider", function($routeProvider) {
	$routeProvider.when("/guest/add", {
		templateUrl : "/resources/partials/guest/add.html"
	}).when("/guest/list", {
		templateUrl : "/resources/partials/guest/list.html"
	}).when("/guest/search", {
		templateUrl : "/resources/partials/guest/search.html"
	}).when("/guest/detail/:id", {
		templateUrl : "/resources/partials/guest/detail.html",
		controller  : "GuestDetailController"
	}).otherwise({
		templateUrl : "/resources/partials/dasboard.html"
	});
} ]);

myModule.config(["gaUID",function(gaUID){
	console.log(gaUID);
	 ga('create', gaUID,  {
		  'cookieDomain': 'none'
	  });
}]);
