/**
 * 
 */
package com.samsoft.wm.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.samsoft.wm.model.Guest;

/**
 * @author Kumar Sambhav Jain
 * 
 */
public interface GuestRepository extends
		PagingAndSortingRepository<Guest, Long> {

}
