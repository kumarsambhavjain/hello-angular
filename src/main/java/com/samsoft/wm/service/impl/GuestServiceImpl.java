/**
 * 
 */
package com.samsoft.wm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.samsoft.wm.model.Guest;
import com.samsoft.wm.repository.GuestRepository;
import com.samsoft.wm.service.GuestService;

/**
 * @author kjai10
 * 
 */
public class GuestServiceImpl implements GuestService {

	@Autowired(required = true)
	private GuestRepository guestRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.samsoft.wm.service.GuestService#save(com.samsoft.wm.model.Guest)
	 */
	@Override
	public Guest save(Guest guest) {
		return guestRepository.save(guest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.samsoft.wm.service.GuestService#findAll()
	 */
	@Override
	public Iterable<Guest> findAll() {
		return guestRepository.findAll();
	}

	@Override
	public Guest get(Long id) {
		return guestRepository.findOne(id);
	}

}
