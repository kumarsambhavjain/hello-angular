/**
 * 
 */
package com.samsoft.wm.service;

import org.springframework.stereotype.Repository;

import com.samsoft.wm.model.Guest;

/**
 * @author kjai10
 * 
 */
@Repository
public interface GuestService {

	public Guest save(Guest guest);

	public Iterable<Guest> findAll();
	
	public Guest get(Long id);
}
