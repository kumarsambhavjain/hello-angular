/**
 * 
 */
package com.samsoft.wm.model;

/**
 * @author Kumar Sambhav Jain
 * 
 */
public enum INVITATION_STATUS {

	ACCEPTED, REJECTED, NO_RESPONSE,NOT_SENT;
}
