/**
 * 
 */
package com.samsoft.wm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * @author Kumar Sambhav Jain
 * 
 */
@Entity
@Table(name = "T_GUEST")
public class Guest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "FULL_NAME", nullable = false, length = 200)
	@Length(min = 3, max = 200)
	private String fullName;

	@Column(name = "EMAIL", nullable = true, length = 100, unique = true)
	private String email;

	@Column(name = "PHONE", nullable = true, length = 12, unique = true)
	private String phone;

	@Column(name = "GUEST_TYPE")
	@NotNull
	private GUEST_TYPE guestType;

	@Column(name = "ACCOMAPNIED_BY")
	private int accompaniedBy;

	@Column(name = "ADDRESS", nullable = true, length = 350)
	private String address;

	@Transient
	private boolean sendInvite;

	@OneToOne(optional = true, orphanRemoval = true)
	private Invitation invitation;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the guestType
	 */
	public GUEST_TYPE getGuestType() {
		return guestType;
	}

	/**
	 * @param guestType
	 *            the guestType to set
	 */
	public void setGuestType(GUEST_TYPE guestType) {
		this.guestType = guestType;
	}

	/**
	 * @return the invitation
	 */
	public Invitation getInvitation() {
		return invitation;
	}

	/**
	 * @param invitation
	 *            the invitation to set
	 */
	public void setInvitation(Invitation invitation) {
		this.invitation = invitation;
	}

	/**
	 * @return the accompaniedBy
	 */
	public int getAccompaniedBy() {
		return accompaniedBy;
	}

	/**
	 * @param accompaniedBy
	 *            the accompaniedBy to set
	 */
	public void setAccompaniedBy(int accompaniedBy) {
		this.accompaniedBy = accompaniedBy;
	}

	/**
	 * @return the sendInvite
	 */
	public boolean isSendInvite() {
		return sendInvite;
	}

	/**
	 * @param sendInvite
	 *            the sendInvite to set
	 */
	public void setSendInvite(boolean sendInvite) {
		this.sendInvite = sendInvite;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

}
