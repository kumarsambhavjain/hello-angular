/**
 * 
 */
package com.samsoft.wm.model;

/**
 * @author Kumar Sambhav Jain
 * 
 */
public enum GUEST_TYPE {

	INDIVIDUAL, FAMILY
}
