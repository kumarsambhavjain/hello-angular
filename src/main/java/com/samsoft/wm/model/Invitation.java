/**
 * 
 */
package com.samsoft.wm.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Kumar Sambhav Jain
 * 
 */
@Entity
@Table(name = "T_INVITATION")
public class Invitation {

	@Id
	private Long id;

	@Column(name = "DATE_CREATED", nullable = false, updatable = false)
	private Date dateCreated;

	@Column(name = "STATUS", nullable = false)
	private INVITATION_STATUS status;

	@Column(name = "COMMENTS")
	private String comments;

	@Column(name = "PROBABILITY", precision = 1, nullable = false)
	private Double probability;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated
	 *            the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the status
	 */
	public INVITATION_STATUS getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(INVITATION_STATUS status) {
		this.status = status;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the probability
	 */
	public Double getProbability() {
		return probability;
	}

	/**
	 * @param probability
	 *            the probability to set
	 */
	public void setProbability(Double probability) {
		this.probability = probability;
	}

}
