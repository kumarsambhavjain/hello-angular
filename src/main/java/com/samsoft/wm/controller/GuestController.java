/**
 * 
 */
package com.samsoft.wm.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.samsoft.wm.model.Guest;
import com.samsoft.wm.service.GuestService;

/**
 * @author Kumar Sambhav Jain
 * 
 */
@Controller
public class GuestController {

	private static final String APPLICATION_JSON = "application/json";

	/**
	 * Logger for GuestController.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GuestController.class);

	@Autowired(required = true)
	private GuestService guestService;

	@RequestMapping(method = RequestMethod.POST, produces = { APPLICATION_JSON }, value = { "/guest/save" })
	public @ResponseBody
	Guest save(@RequestBody Guest guest) {
		LOGGER.debug("Create guest start");
		guestService.save(guest);
		return guest;
	}

	@RequestMapping(value = { "/guest/list" }, method = { RequestMethod.GET }, produces = { APPLICATION_JSON })
	public @ResponseBody
	Iterable<Guest> list() {
		LOGGER.debug("Returning guest List");
		return guestService.findAll();
	}

	@RequestMapping(value = { "/guest/detail/{id}" }, method = { RequestMethod.GET }, produces = { APPLICATION_JSON })
	@ResponseBody
	public Guest detail(@PathVariable Long id) {
		return guestService.get(id);
	}
}
