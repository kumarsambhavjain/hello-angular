/**
 * 
 */
package com.samsoft.wm.controller.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author kjai10
 * 
 */
@ControllerAdvice
public class DataIntegrityViolationExceptionHandler {

	/**
	 * Logger for GuestController.
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DataIntegrityViolationExceptionHandler.class);

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public void handleConflict(DataIntegrityViolationException e) {
		LOGGER.info("Error while performaing database operation", e);
	}
}
